# Jak vytvorit venv

## Windows

    pip install virtualenv
    virtualenv venv
    .\venv\Scripts\activate
    pip install -r requirements.txt

## Mac

    pip install virtualenv
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt